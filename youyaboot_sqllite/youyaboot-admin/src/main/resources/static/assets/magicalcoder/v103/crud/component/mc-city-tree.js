/**
* 城市树组件
 * 请根据您自己的城市表结构 稍微改造下 init myDataToZtreeData ztreeDataToMyData方法即可
* by www.magicalcoder.com
 * 如何初始化复选框树 new CityTree({treeId:"cityTree",chkboxType:{ "Y" : "ps", "N" : "ps" }})
 * 然后初始化单选按钮树 new CityTree({treeId:"cityTree",chkStyle:"radio",radioType:"all",defaultSelectIds: [entity.cityStructureId]})
 * 页面请引入
 *  样式<link rel="stylesheet" th:href="@{/assets/magicalcoder/v103/ztree3/css/zTreeStyle/zTreeStyle.css}" media="all" />
 *  js      <script>
             var jQuery = layui.jquery;
         </script>
         <script type="text/javascript"  th:src="@{assets/magicalcoder/v103/ztree3/js/jquery.ztree.all.js}"></script>
         <script type="text/javascript"  th:src="@{/assets/magicalcoder/v103/crud/component/mc-city-tree.js}"></script>
 *
*/
var $ = layui.jquery;
var layer = layui.layer;
function CityTree(params) {
    this.params = {
        treeId:"",//页面放置一个 <ul id="cityTree" class="ztree"></ul>标签即可
        chkboxType:"",//{ "Y" : "ps", "N" : "ps" } 级联
        chkStyle:"",//radio
        radioType:"",//all level
        defaultSelectIds:[]//默认选择的id
    }
    $.extend(true,this.params,params);
    this.cityTreeObj = null;
    this.treeSetting = null;
    this.settings();
    this.init();
}

/**
 * 把我的数据转换成ztree的数据
 * @param dbEntity
 * @returns {{id: number, pId: number, open: boolean, name: *, nodeType: 自定义数据}}
 */
CityTree.prototype.myDataToZtreeData = function(dbEntity){
    return {id:parseInt(dbEntity.id),pId:parseInt(dbEntity.parentId),open:false,name:dbEntity.cityName,nodeType:dbEntity.nodeType};
}
/**
 * 把ztree数据转换成我的数据库数据
 * @param ztreeNode
 * @returns {{id: *, cityName: *, parentId: number | *, open: *, nodeType: *}}
 */
CityTree.prototype.ztreeDataToMyData = function(ztreeNode){
    return {id:ztreeNode.id,cityName:ztreeNode.name,parentId:ztreeNode.pId,open:ztreeNode.open,nodeType:ztreeNode.nodeType};
}

CityTree.prototype.settings = function () {
    var _t = this;
    this.treeSetting = {
        check: {
            enable: true,
            /*chkboxType : { "Y" : "ps", "N" : "ps" },*/
            /*chkStyle: "radio",
            radioType:"all"*/
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onClick: function (e, treeId, treeNode, clickFlag) {
                _t.cityTreeObj.checkNode(treeNode, !treeNode.checked, true);
            }
        }
    };

    if(this.params.chkboxType!=''){
        this.treeSetting.check.chkboxType = this.params.chkboxType;
    }
    if(this.params.chkStyle!=''){
        this.treeSetting.check.chkStyle = this.params.chkStyle;
    }
    if(this.params.radioType!=''){
        this.treeSetting.check.radioType = this.params.radioType;
    }


}

CityTree.prototype.dataAdapter = function(dbData){
    var defaultSelectIdMap = {}
    for(var i =0;i<this.params.defaultSelectIds.length;i++){
        defaultSelectIdMap[this.params.defaultSelectIds[i]+""] = "";
    }

    var zNodes = []
    if(typeof dbData != 'undefined'){
        for(var i=0;i<dbData.length;i++){
            var city = dbData[i];
            var item = this.myDataToZtreeData(city)
            if(city.nodeTopclass=='1' ){
                item.open = true;
            }
            var checked = typeof defaultSelectIdMap[item.id+""] != 'undefined';
            if(checked){
                item.checked = checked;
                item.open = true;
            }
            zNodes.push(item);
        }
    }
    return zNodes;
}
/**
 * ajax获取管理后台城市表的所有数据 请保证是个pid,parentId的表 直接简单返回list即可 无需嵌套的结构 适当更换url成您自己的
 */
CityTree.prototype.init = function () {
    var _t = this;
    $.getJSON("admin/city/city_data",{time:new Date().getTime()},function (data) {
        if(data.flag){
            var list = data.data;
            $.fn.zTree.init($("#"+_t.params.treeId), _t.treeSetting, _t.dataAdapter(list));
            _t.cityTreeObj = $.fn.zTree.getZTreeObj(_t.params.treeId);
        }else {
            layer.msg(data.desc)
        }
    })

}

/*如果是一个radio*/
CityTree.prototype.getSingleRadioSelectNodeAndChilrenData = function () {
    var radioNodes = this.cityTreeObj.getCheckedNodes(true);
    if(radioNodes!=null && typeof radioNodes !='undefined'){
        var node = radioNodes[0];
        return this.cityTreeObj.transformToArray(node);
    }

    return [];
}
/*如果是radio 获取选中的这些radio的数据*/
CityTree.prototype.getSingleRadioSelectNodeData = function () {
    var data = []
    var radioNodes = this.cityTreeObj.getCheckedNodes(true);
    if(radioNodes!=null && typeof radioNodes !='undefined'){
        for(var i=0;i<radioNodes.length;i++){
            var node = radioNodes[i];
            data.push(this.ztreeDataToMyData(node))
        }
    }
    return data;
}

/*如果是checkbox 获取选择的所有节点数据*/
CityTree.prototype.getFullCheckedNodesData = function () {
    var data = []
    var checkedNodes = this.cityTreeObj.getCheckedNodes(true);
    if(checkedNodes!=null && typeof checkedNodes !='undefined'){
        for(var i=0;i<checkedNodes.length;i++){
            var node = checkedNodes[i];
            var status = node.getCheckStatus();
            if(status.checked && !status.half){
                data.push(this.ztreeDataToMyData(node))
            }
        }
    }
    return data;
}

